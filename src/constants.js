export const colors = {
    "primary-color": "#ffa400",
    "giraffe-success": "#00a860",
    "giraffe-primary": "#1b00b0",
    "giraffe-danger": "#f45b05",
    "giraffe-dark": "#2b2a2a",
    "giraffe-light": "#b5cded",
    "giraffe-secondary": "#c3bbad",
    "giraffe-background": "lightgrey",
  };