import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import {  
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,  
  UncontrolledDropdown,
} from "reactstrap";
import PropTypes from "prop-types";

import { AppNavbarBrand } from "@coreui/react";
import logo from "../../assets/img/brand/logo.svg";
import ico from "../../assets/img/brand/ico.svg";
import NavItem from "reactstrap/lib/NavItem";


const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {            
      let nav = (
        <Nav className="d-md-down" navbar>
          <NavItem className="px-3">
            <Link to="/lms/categories" className="nav-link">
            Categories
            </Link>
            
          </NavItem>
          <NavItem className="px-3">
            <Link to="/lms/courses" className="nav-link">
            Courses
            </Link>            
          </NavItem>        
        </Nav>
      );
    
    return (
      <React.Fragment>
        {/*<AppSidebarToggler className="d-lg-none" display="md" mobile />*/}
        <div className="d-md-down-none">
          <AppNavbarBrand
            full={{ src: logo, width: 300, height: 45, alt: "Giraffe logo" }}
            minimized={{ src: ico, width: 30, height: 30, alt: "Giraffe Logo" }}
          />
        </div>

        {nav}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default withRouter(DefaultHeader);
