import React from "react";
const CategoriesView = React.lazy(() =>
  import("./views/LMS/Categories/CategoriesView/CategoriesView")
);
const CategoriesForm = React.lazy(() =>
  import("./views/LMS/Categories/CategoriesForm/CategoriesForm")
);
const CoursesView = React.lazy(() =>
  import("./views/LMS/Courses/CoursesView/CoursesView")
);
const CoursesForm = React.lazy(() =>
  import("./views/LMS/Courses/CoursesForm/CoursesForm")
);

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  
  {
    path: "/lms/categories/edit/:id",
    name: "Edit category",
    component: CategoriesForm,
    required_admin: false,
  },
  {
    path: "/lms/categories/add",
    name: "Add category",
    component: CategoriesForm,
    required_admin: false,
  },
  {
    path: "/lms/categories",
    name: "Categories",
    component: CategoriesView,
    required_admin: false,
  },
  {
    path: "/lms/courses/edit/:id",
    name: "Edit Course",
    component: CoursesForm,
    required_admin: false,
  },
  {
    path: "/lms/courses/add",
    name: "Add Course",
    component: CoursesForm,
    required_admin: false,
  },
  {
    path: "/lms/courses",
    name: "Courses",
    component: CoursesView,
    required_admin: false,
  }
];

export default routes;
