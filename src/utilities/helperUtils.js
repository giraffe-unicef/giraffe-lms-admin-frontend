import ProvinceCodes from "../enums/ProvinceCodes";

export function processAddressComponents(addressComponents) {
    const namePerAddressType = {};
    addressComponents.forEach(component => {
      component.types.forEach(
        type => (namePerAddressType[type] = component.short_name)
      );
    });
  
    const street_number =
      namePerAddressType["street_number"] !== undefined
        ? namePerAddressType["street_number"]
        : "";
  
    const route =
      namePerAddressType["route"] !== undefined
        ? namePerAddressType["route"]
        : "";
  
    const suburb =
      namePerAddressType["sublocality"] !== undefined
        ? namePerAddressType["sublocality"]
        : "";
  
    const city =
      namePerAddressType["locality"] !== undefined
        ? namePerAddressType["locality"]
        : "";
  
    const postalCode =
      namePerAddressType["postal_code"] !== undefined
        ? namePerAddressType["postal_code"]
        : "";
  
    const provinceId =
      namePerAddressType["administrative_area_level_1"] !== undefined
        ? ProvinceCodes[namePerAddressType["administrative_area_level_1"]]
        : null;
  
    const processedAddressComponents = {
        streetName: `${street_number} ${route}`.trim(),
      suburb,
      city,
      provinceId,
      postalCode
    };
    return processedAddressComponents;
  }