import { toast } from 'react-toastify';
export default function showNotification (message, options)  {
    toast(message, options);
}