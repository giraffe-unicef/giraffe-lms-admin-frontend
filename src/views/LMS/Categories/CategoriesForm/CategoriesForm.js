import Axios from "axios";
import React, { Component } from "react";
import LoadingAnimation from "../../../../utilities/LoadingAnimation";
import { Button, Card, CardBody, Col, Row } from "reactstrap";
import StandardInput from "../../../../form_components/StandardInput/StandardInput";
import StandardTextArea from "../../../../form_components/StandardTextArea/StandardTextArea";
import CategoriesFormValidation from "./CategoriesFormValidation";
import validationUtils from "../../../../utilities/validationUtils";
import showNotification from "../../../../utilities/notificationUtils";
import Dropzone from "react-dropzone";
import "./categories-form.scss";
import { result } from "lodash";
import { Link } from "react-router-dom";
export default class CategorieForm extends Component {
  state = {
    name: "",
    description: "",
    color: "",
    logo_path: "",
    showLoading: false,
    categoryId: null,
    logo: {},
  };
  validations = CategoriesFormValidation;
  constructor(props) {
    super(props);
    this.state.showLoading = this.props.match.params.id ? true : false;
    this.state.categoryId = this.props.match.params.id;
  }
  componentWillMount() {
    if (this.state.categoryId) {
      Axios({
        url:
          process.env.REACT_APP_LMS_API_URL +
          "/admin/category/" +
          this.state.categoryId,
        method: "get",
      })
        .then((res) => {
          if (res.data.published) {
            this.props.history.push("/lms/categories");
          }
          this.setState({
            showLoading: false,
            ...res.data,
          });
        })
        .catch((err) => {
          this.props.history.push("/lms/categories");
          console.log(err);
        });
    }
  }

  updateStateVariable = (value, key, options = {}) => {    
    if(options.length){
      value = value.slice(0,options.length);
    }    
    this.setState({
      [key]: value
    });
  };

  save = async () => {
    if (!validationUtils.checkFormValidity(this.state, this.validations)) {
      this.setState({
        showError: true,
      });
      return;
    }
    this.setState({
      showLoading: true,
    });
    
    

    let requestObj = {};
    let requestData = {
      name: this.state.name,
      description: this.state.description,
      color: this.state.color,
      logo: this.state.logo.file ? this.state.logo.file : null,
      file_type: this.state.logo.file_type ? this.state.logo.file_type : null,
    };
    if (this.state.categoryId) {
      requestObj = {
        url:
          process.env.REACT_APP_LMS_API_URL +
          "/admin/category/" +
          this.state.categoryId,
        method: "put",
        data: requestData,
      };
    } else {
      requestObj = {
        url: process.env.REACT_APP_LMS_API_URL + "/admin/category",
        method: "post",
        data: requestData,
      };
    }

    Axios(requestObj)
      .then(() => {
        if (this.state.categoryId) {
          showNotification("category updated successfully.", {
            type: "success",
          });
        } else {
          showNotification("category added successfully.", { type: "success" });
        }
        this.props.history.push("/lms/categories");
      })
      .catch((err) => {
        if (this.state.categoryId) {
          showNotification("Error updating category", { type: "error" });
        } else {
          showNotification("Error adding category", { type: "error" });
        }
        this.setState({
          showLoading: false,
        });
      });
  };
  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log(reader.result);
        this.setState({
          logo: {
            file: reader.result.split("base64,")[1],
            file_type: file.type.split("/")[1],
          },
        });
      };
      reader.onerror = (error) => reject(error);
    });

  render() {
    if (this.state.showLoading) {
      return <LoadingAnimation />;
    }

    return (
      <div className="animated fadeIn categories-form">
        <Card>
          <CardBody>
            <Row className="align-items-center">
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <StandardInput
                  value={this.state.name}
                  onChange={(value) => this.updateStateVariable(value, "name")}
                  label="Name"
                  placeholder="Enter Name"
                  validations={this.validations.name}
                  showError={this.state.showError}
                />
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <StandardTextArea
                  label="Description"
                  placeholder="Enter Description"
                  value={this.state.description}
                  onChange={(value) =>
                    this.updateStateVariable(value, "description", {length : 210})
                  }
                  rows={2}
                  validations={this.validations.description}
                  showError={this.state.showError}
                />
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <StandardInput
                  value={this.state.color}
                  onChange={(value) => this.updateStateVariable(value, "color")}
                  label="Color"
                  placeholder="Enter Color"
                  validations={this.validations.color}
                  showError={this.state.showError}
                />
              </Col>
              <Col col="8" xs="8" md="8" className="mb-3">
                <Dropzone
                  accept="image/*"
                  multiple={false}
                  onDrop={(file) => {
                    if (file[0]) {
                      this.toBase64(file[0]);
                      this.setState({
                        logo_path: URL.createObjectURL(file[0]),
                      });
                    }
                  }}
                >
                  {({ getRootProps, getInputProps }) => (
                    <section>
                      <div
                        {...getRootProps()}
                        className="drop-zone d-flex align-items-center justify-content-center"
                      >
                        <input {...getInputProps()} />
                        <div>
                          Drag 'n' drop image here, or click to select file
                        </div>
                      </div>
                    </section>
                  )}
                </Dropzone>
                {this.state.showError && !this.state.logo_path ? (
                    <div className="danger-text">
                      Logo is Required
                    </div>
                  ) : null}
              </Col>
              <Col col="4" xs="4" md="4" className="mb-3">
                {this.state.logo_path ? (
                  <img src={this.state.logo_path} alt="logo" className="logo" />
                ) : null}
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <Link to="/lms/categories">
                  <Button outline color="danger" className="mr-3">
                    Back
                  </Button>
                </Link>
                <Button outline color="primary" onClick={this.save}>
                  Save
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}
