import ValidationEnum from "../../../../enums/ValidationsEnum";
export default  {
  name: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Name is required.",
    },
  ],
  description: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Description is required.",
    },
  ],
  color: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Color is required.",
    },
  ]  
};
