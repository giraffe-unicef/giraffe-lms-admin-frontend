import React, { Component } from "react";
import Axios from "axios";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import LoadingAnimation from "../../../../utilities/LoadingAnimation";
import { Button, Card, CardBody, Col, Row } from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import moment from "moment";
import "./categories-view.scss";
import { Link } from "react-router-dom";
import showNotification from "../../../../utilities/notificationUtils";
export default class CategoriesView extends Component {
  state = {
    showLoading: true,
    categories: [],
  };
  componentWillMount() {
    Axios({
      url: process.env.REACT_APP_LMS_API_URL + "/admin/category",
      method: "get",
    })
      .then((res) => {
        this.setState({
          showLoading: false,
          categories: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  togglePublish = (row, index) => {
    this.setState({
      showLoading: true,
    });
    Axios({
      url: process.env.REACT_APP_LMS_API_URL + "/admin/category/" + row.id,
      method: "patch",
      data: {
        published: !row.published,
      },
    })
      .then((res) => {
        let categories = this.state.categories;
        for(let category of categories){
          if(category.id === row.id){
            category.published = !row.published
            break;
          }
        }
        if (!row.published) {
          showNotification("Category unpublished successfully.", {
            type: "success",
          });
        } else {
          showNotification("Category published successfully.", {
            type: "success",
          });
        }

        this.setState({
          showLoading: false,
          categories: categories,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  dateFormatter(cell) {
    return <div>{moment(cell).format("DD/MM/YYYY HH:mm")}</div>;
  }

  publishedFormatter(cell, row) {
    if (row.published) {
      return <div>{moment(cell).format("DD/MM/YYYY HH:mm")}</div>;
    } else {
      return <div className="danger-text">Not Published</div>;
    }
  }

  logoFormatter(cell) {
    if (!cell) {
      return null;
    }
    return (
      <div className="img-container">
        <img src={cell} alt="Category logo" />
      </div>
    );
  }

  actionsFormatter(cell, row, index) {
    let publishButtonText = row.published ? "Unpublish" : "Publish";
    let editBtn;
    if (!row.published) {
      editBtn = (
        <div>
          <Link
            to={"/lms/categories/edit/" + row.id}
            style={{ textDecoration: "none" }}
          >
            <Button
              renderas="button"
              block
              outline
              color="primary"
              className="action-btn"
            >
              Edit
            </Button>
          </Link>
        </div>
      );
    }
    return (
      <div className="d-flex justify-content-between">
        <div>
          <Button
            className="action-btn"
            renderas="button"
            block
            outline
            color="primary"
            onClick={() => this.togglePublish(row, index)}
          >
            {publishButtonText}
          </Button>
        </div>
        {editBtn}
      </div>
    );
  }

  render() {
    if (this.state.showLoading) {
      return <LoadingAnimation />;
    }
    const paginationSize = 10;
    let { categories } = this.state;
    let pagination = null;
    const { SearchBar } = Search;

    if (categories.length > paginationSize) {
      console.log("APplying pagination");
      pagination = paginationFactory();
    }

    let colStyle = {
      verticalAlign: "middle",
    };
    let columns = [
      {
        id: 0,
        dataField: "id",
        hidden: true,
      },
      {
        id: 1,
        dataField: "name",
        text: "Name",
        style: colStyle,
        sort: true,
      },
      {
        id: 2,
        dataField: "description",
        text: "Description",
        style: colStyle,
        sort: true,
      },
      {
        id: 3,
        dataField: "color",
        text: "Color",
        style: colStyle,
        sort: true,
      },
      {
        id: 4,
        dataField: "created_date",
        text: "Created Date",
        style: colStyle,
        sort: true,
        formatter: this.dateFormatter,
      },
      {
        id: 5,
        dataField: "created_date",
        text: "Published Date",
        style: colStyle,
        sort: true,
        formatter: this.publishedFormatter,
      },
      {
        id: 6,
        dataField: "logo_path",
        text: "Logo",
        style: colStyle,
        sort: true,
        formatter: this.logoFormatter,
      },
      {
        id: 7,
        text: "Actions",
        style: colStyle,
        sort: true,
        formatter: (cell, row, index) =>
          this.actionsFormatter(cell, row, index),
      },
    ];
    return (
      <div className="animated fadeIn categories-view">
        <Card>
          <CardBody>
            <Row className="align-items-center">
              <Col col="12" xl className="mb-3 mb-xl-0">
                <ToolkitProvider
                  keyField="id"
                  data={categories}
                  columns={columns}
                  style={{ overflowX: "scroll" }}
                  search
                >
                  {(props) => (
                    <div>
                      <Row className="align-items-center">
                        <Col xs="8">
                          <h2>Categories</h2>
                        </Col>
                        
                        <Col xs="3">
                          <SearchBar {...props.searchProps} />                          
                        </Col>
                        <Col xs="1">                          
                          <Link
                            to={"/lms/categories/add/"}
                            style={{ textDecoration: "none" }}
                          >
                            <Button
                              renderas="button"
                              block
                              outline
                              color="primary"
                              className="action-btn float-right "
                            >
                              Add
                            </Button>
                          </Link>
                        </Col>
                      </Row>
                      <hr />
                      <BootstrapTable
                        {...props.baseProps}
                        pagination={pagination}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}
