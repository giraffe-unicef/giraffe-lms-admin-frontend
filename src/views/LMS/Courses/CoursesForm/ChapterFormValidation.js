import ValidationEnum from "../../../../enums/ValidationsEnum";
export default  {
  name: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Name is required.",
    },
  ],
  content: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Content is required.",
    },
  ],
  chronology: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Chronology is required.",
    },
  ]  
};
