import * as React from 'react';
import { List, arrayMove } from 'react-movable';
 
const ChaptersList= ({chapters,renderChapter}) => {
    
  const [items, setItems] = React.useState(chapters);
  return (
    <List
      values={items}
      onChange={({ oldIndex, newIndex }) =>
        setItems(arrayMove(items, oldIndex, newIndex))
      }
      renderList={({ children, props }) => <ul {...props}>{children}</ul>}
      renderItem={({ value, props }) => <li {...props}>{renderChapter(value,props)}</li>}
    />
  );
};


export default ChaptersList;