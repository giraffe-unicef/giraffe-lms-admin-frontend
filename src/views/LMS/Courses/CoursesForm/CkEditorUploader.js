import Axios from "axios";

export default class CkEditorUploader {
  constructor(loader) {
    // CKEditor 5's FileLoader instance.
    this.loader = loader;

    // URL where to send files.
    this.url = "https://example.com/image/upload/path";
  }

  // Starts the upload process.
  upload() {
    return new Promise((resolve, reject) => {
      this.loader.file.then((file) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.sendRequest(resolve,reject, reader.result.split("base64,")[1], file.type.split("/")[1]);          
        };
        reader.onerror = (error) => reject(error);
      });
    });
  }

  sendRequest = async (resolve, reject, file, file_type) => {
    let requestObj = {
      url:
        process.env.REACT_APP_LMS_API_URL +
        "/file/upload",
      method: "post",
      data: {
        file: file,
        file_type: file_type
      },
    };

    Axios(requestObj)
      .then((response) => {
          
        resolve({default : response.data});
      })
      .catch((err) => {
        reject(err);
      });
  };
}
