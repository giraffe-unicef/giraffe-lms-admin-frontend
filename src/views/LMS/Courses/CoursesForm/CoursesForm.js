import Axios from "axios";
import React, { Component } from "react";
import LoadingAnimation from "../../../../utilities/LoadingAnimation";
import { Button, Card, CardBody, Col, Row, CardHeader } from "reactstrap";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

import StandardInput from "../../../../form_components/StandardInput/StandardInput";
import StandardTextArea from "../../../../form_components/StandardTextArea/StandardTextArea";
import CoursesFormValidation from "./CoursesFormValidation";
import validationUtils from "../../../../utilities/validationUtils";
import showNotification from "../../../../utilities/notificationUtils";
import Dropzone from "react-dropzone";
import StandardSelect from "../../../../form_components/StandardSelect/StandardSelect";
import "./courses-form.scss";
import ChapterFormValidation from "./ChapterFormValidation";
import CkEditorUploader from "./CkEditorUploader";
import { Link } from "react-router-dom";
export default class CourseForm extends Component {
  state = {
    name: "",
    overview: "",
    file: "",
    showLoading: true,
    categoryId: null,
    courseId: null,
    chapters: [{ expanded: true }],
    logo: {},
    deletedChapters: [],
  };
  validations = CoursesFormValidation;
  chapterValidations = ChapterFormValidation;
  constructor(props) {
    super(props);
    this.state.courseId = this.props.match.params.id;
  }
  componentDidMount() {
    let requests = [
      Axios({
        url: process.env.REACT_APP_LMS_API_URL + "/public/category/",
        method: "get",
      }),
    ];

    if (this.state.courseId) {
      requests.push(
        Axios({
          url:
            process.env.REACT_APP_LMS_API_URL +
            "/admin/course/" +
            this.state.courseId,
          method: "get",
        })
      );
    }
    Promise.all(requests)
      .then((res) => {
        let categories = res[0].data.map((cat) => {
          return { value: cat.id, label: cat.name };
        });
        if (this.state.courseId) {
          res[1].data.categoryId = res[1].data.category_id;
          res[1].data.logo_path = res[1].data.file_url;
          res[1].data.chapters = res[1].data.chapters.sort((a, b) => {
            return a.chronology - b.chronology;
          });
          for (let chapter of res[1].data.chapters) {
            chapter.expanded = true;
          }
          this.setState({
            showLoading: false,
            ...res[1].data,
            categories: categories,
          });
        } else {
          this.setState({
            showLoading: false,
            categories: categories,
          });
        }
      })
      .catch((err) => {
        this.props.history.push("/lms/courses");
        console.log(err);
      });
  }

  updateIfNumber = (value, key) => {
    if (!isNaN(value)) {
      this.updateStateVariable(parseInt(value), key);
    }
  };

  updateStateVariable = (value, key) => {
    this.setState({
      [key]: value,
    });
  };

  updateChapterVariable = (value, key, index) => {
    let chapters = this.state.chapters;
    chapters[index][key] = value;
    this.setState({
      chapters: chapters,
    });
  };
  updateChapterVariableIfNumber = (value, key, index) => {
    if (isNaN(value)) {
      return;
    }
    let chapters = this.state.chapters;
    chapters[index][key] = parseInt(value);
    this.setState({
      chapters: chapters,
    });
  };

  save = async () => {
    let chapters = this.state.chapters;
    if (!validationUtils.checkFormValidity(this.state, this.validations)) {
      this.setState({
        showError: true,
      });
      return;
    }
    for (let i = 0; i < chapters.length; i++) {
      chapters[i].chronology = i + 1;
      if (
        !validationUtils.checkFormValidity(chapters[i], this.chapterValidations)
      ) {
        this.selectChapter(i);
        this.setState({
          showError: true,
        });
        document.getElementById("chapter-" + i).scrollIntoView();
        return;
      }
    }
    this.setState({
      showLoading: true,
    });

    let requestObj = {};
    let requestData = {
      name: this.state.name,
      overview: this.state.overview,
      file: "this.state.logo.file ? this.state.logo.file : undefined",
      file_type: this.state.logo.file_type
        ? this.state.logo.file_type
        : undefined,
      category_id: this.state.categoryId,
      chapters: chapters,
      deleted_chapters: this.state.deletedChapters,
    };
    if (this.state.courseId) {
      requestObj = {
        url:
          process.env.REACT_APP_LMS_API_URL +
          "/admin/course/" +
          this.state.courseId,
        method: "put",
        data: requestData,
      };
    } else {
      requestObj = {
        url: process.env.REACT_APP_LMS_API_URL + "/admin/course",
        method: "post",
        data: requestData,
      };
    }

    Axios(requestObj)
      .then(() => {
        if (this.state.courseId) {
          showNotification("Course updated successfully.", {
            type: "success",
          });
        } else {
          showNotification("Course added successfully.", { type: "success" });
        }
        this.props.history.push("/lms/courses");
      })
      .catch((err) => {
        if (this.state.courseId) {
          showNotification("Error updating course", { type: "error" });
        } else {
          showNotification("Error adding course", { type: "error" });
        }
        this.setState({
          showLoading: false,
        });
      });
  };
  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log(reader.result);
        this.setState({
          logo: {
            file: reader.result.split("base64,")[1],
            file_type: file.type.split("/")[1],
          },
        });
      };
      reader.onerror = (error) => reject(error);
    });

  addChapter = () => {
    this.state.chapters.push({
      id: null,
      name: "",
      chronology: "",
      content: "",
      expanded: true,
    });
    this.forceUpdate();
  };
  selectChapter = (index) => {
    let chapters = this.state.chapters;
    chapters[index].expanded = !chapters[index].expanded;
    this.setState({ chapters: chapters });
  };
  deleteChapter = (index) => {
    if (
      window.confirm(
        "Are you sure you want delete this chapter? You won't be able to restore it back"
      )
    ) {
      if (this.state.chapters[index].id) {
        this.state.deletedChapters.push(this.state.chapters[index].id);
      }

      this.state.chapters.splice(index, 1);
      this.forceUpdate();
    }
  };

  MyCKUploadAdapterPlugin(editor) {
    editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
      return new CkEditorUploader(loader);
    };
  }
  moveChapter = (index, move) => {
    if (index + move <= 0) {
      return;
    }
    if (index + move >= this.state.chapters) {
      return;
    }
    let chapters = this.state.chapters;
    var element = chapters[index];
    chapters.splice(index, 1);
    chapters.splice(index + move, 0, element);
    this.setState({
      chapters: chapters,
    });
  };
  renderChapters = () => {
    return this.state.chapters.map((chapter, index) => {
      return (
        <Card id={`chapter-${index}`}>
          <CardHeader>
            <div className="d-block">
              <Button
                outline
                color="warning"
                onClick={() => this.moveChapter(index, -1)}
                className="mr-3"
                disabled={index === 0}
              >
                Move <i className="fa fa-chevron-up"></i>
              </Button>

              <Button
                outline
                color="warning"
                onClick={() => this.moveChapter(index, 1)}
                className="mr-3"
                disabled={index === this.state.chapters.length - 1}
              >
                Move <i className="fa fa-chevron-down"></i>
              </Button>

              <StandardInput
                className="d-inline-block chapter-name"
                value={chapter.name}
                onChange={(value) =>
                  this.updateChapterVariable(value, "name", index)
                }                
                placeholder="Chapter Name"
                validations={this.chapterValidations.name}
                showError={this.state.showError}
                block={false}
              />

              {chapter.expanded ? (
                <i
                  className="fa fa-chevron-up float-right mt-2"
                  onClick={() => this.selectChapter(index)}
                ></i>
              ) : (
                <i
                  className="fa fa-chevron-down float-right mt-2"
                  onClick={() => this.selectChapter(index)}
                ></i>
              )}
              <Button
                outline
                color="danger"
                onClick={() => this.deleteChapter(index)}
                className="mr-3 float-right"
              >
                Delete Chapter
              </Button>
            </div>
          </CardHeader>
          {chapter.expanded ? (
            <CardBody>
              <Row>
                <Col col="12" xs="12">
                  <label className="font-weight-bold mb-2">Content</label>
                  <CKEditor
                    config={{
                      extraPlugins: [this.MyCKUploadAdapterPlugin],
                    }}
                    editor={ClassicEditor}
                    data={chapter.content}
                    onReady={(editor) => {
                      // You can store the "editor" and use when it is needed.
                      console.log("Editor is ready to use!", editor);
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      console.log("Blur.", data);
                      this.updateChapterVariable(data, "content", index);
                    }}
                    onBlur={(event, editor) => {
                      const data = editor.getData();
                      console.log("Blur.", data);
                    }}
                    onFocus={(event, editor) => {
                      const data = editor.getData();
                      console.log("Focus.", data);
                    }}
                  />
                  {this.state.showError && !chapter.content ? (
                    <div className="danger-text">
                      Chapter content is Required
                    </div>
                  ) : null}
                </Col>
              </Row>
            </CardBody>
          ) : null}
        </Card>
      );
    });
  };
  render() {
    if (this.state.showLoading) {
      return <LoadingAnimation />;
    }
    return (
      <div className="animated fadeIn courses-form">
        <Card>
          <CardBody>
            <Row className="align-items-center">
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <StandardInput
                  value={this.state.name}
                  onChange={(value) => this.updateStateVariable(value, "name")}
                  label="Name"
                  placeholder="Enter Name"
                  validations={this.validations.name}
                  showError={this.state.showError}
                />
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <StandardTextArea
                  label="Overview"
                  placeholder="Enter overview"
                  value={this.state.overview}
                  onChange={(value) =>
                    this.updateStateVariable(value, "overview")
                  }
                  rows={2}
                  validations={this.validations.overview}
                  showError={this.state.showError}
                />
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <StandardSelect
                  className="mb-3"
                  label="Category"
                  placeholder="Select Category"
                  options={this.state.categories}
                  value={this.state.categoryId}
                  onChange={(value) =>
                    this.updateStateVariable(value, "categoryId")
                  }
                  validations={this.validations.categoryId}
                  showError={this.state.showError}
                />
              </Col>
              <Col col="8" xs="8" md="8" className="mb-3">
                <Dropzone
                  accept="image/*"
                  multiple={false}
                  onDrop={(file) => {
                    if (file[0]) {
                      this.toBase64(file[0]);
                      this.setState({
                        logo_path: URL.createObjectURL(file[0]),
                      });
                    }
                  }}
                >
                  {({ getRootProps, getInputProps }) => (
                    <section>
                      <div
                        {...getRootProps()}
                        className="drop-zone d-flex align-items-center justify-content-center"
                      >
                        <input {...getInputProps()} />
                        <div>
                          Drag 'n' drop image here, or click to selectfile
                        </div>
                      </div>
                    </section>
                  )}
                </Dropzone>
              </Col>
              <Col col="4" xs="4" md="4" className="mb-3">
                {this.state.logo_path ? (
                  <img src={this.state.logo_path} alt="logo" className="logo" />
                ) : null}
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <Card>
                  <CardHeader>
                    <Row>
                      <Col col="12">
                        <div className="d-block">
                          <h3 className="d-inline"> Chapters</h3>

                          <Button
                            outline
                            color="primary"
                            onClick={this.addChapter}
                            className="float-right"
                          >
                            Add Chapter
                          </Button>
                        </div>
                      </Col>
                    </Row>
                  </CardHeader>

                  <CardBody>{this.renderChapters()}</CardBody>
                </Card>
              </Col>
              <Col col="12" xs="12" md="12" className="mb-3 mb-xl-0">
                <Link to="/lms/courses">
                  <Button outline color="danger" className="mr-3">
                    Back
                  </Button>
                </Link>

                <Button outline color="primary" onClick={this.save}>
                  Save
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}
