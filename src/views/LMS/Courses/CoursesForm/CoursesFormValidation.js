import ValidationEnum from "../../../../enums/ValidationsEnum";
export default  {
  name: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Name is required.",
    },
  ],
  overview: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Overview is required.",
    },
  ],
  categoryId: [
    {
      type: ValidationEnum.REQUIRED,
      message: "Category ID is required.",
    },
  ]    
};
